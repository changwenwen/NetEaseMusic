src.plugin.NetEase package
==========================

Submodules
----------

src.plugin.NetEase.api module
-----------------------------

.. automodule:: src.plugin.NetEase.api
    :members:
    :undoc-members:
    :show-inheritance:

src.plugin.NetEase.normalize module
-----------------------------------

.. automodule:: src.plugin.NetEase.normalize
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src.plugin.NetEase
    :members:
    :undoc-members:
    :show-inheritance:
